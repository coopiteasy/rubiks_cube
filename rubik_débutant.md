# résoudre le rubik’s cube : méthode débutant

cette méthode permet de résoudre le rubik’s cube de manière assez facile, sans devoir mémoriser un grand nombre de formules.

elle se compose de 7 étapes.
le cube se résout en commençant par une face, et ensuite on avance étage par étage.

## préliminaires

### présentation du cube

le rubik’s cube est composé de 6 faces, elles-mêmes composées de 9 facettes.
on l’appelle aussi cube 3 × 3 × 3, car chaque côté est composé de 3 plus petits cubes.

il a en tout 26 pièces visibles :

*   6 centres : d’une seule couleur chacune, ce sont les seules pièces fixes visibles et elles définissent la couleur de la face sur laquelle elles se trouvent
*   12 arêtes : de 2 couleurs, elles se trouvent à l’intersection de 2 faces
*   8 sommets : de 3 couleurs, elles se trouvent à l’intersection de 3 faces

chaque pièce est unique, il n’y a aucune pièce avec les mêmes couleurs qu’une autre.

### notation des mouvements

pour décrire les mouvements, il existe une notation conventionnelle.
chaque face du cube est identifiée par une lettre :

*   **U** (pour “*up*”) est la face du dessus
*   **D** (pour “*down*”) est la face du dessous
*   **L** (pour “*left*”) est la face de gauche
*   **R** (pour “*right*”) est la face de droite
*   **F** (pour “*front*”) est la face de devant
*   **B** (pour “*back*”) est la face de derrière

les mouvements sont décrits par une suite de ces lettres :

*   une lettre seule représente un quart de tour de la face dans le sens horloger/anti-trigonométrique.
*   une lettre suivie d’une apostrophe (**'**), prononcé “prime”, représente la même chose mais dans l’autre sens (anti-horloger/trigonométrique).
*   une lettre suivie d’un **2** représente un demi-tour de la face.

### “*sexy move*”

on appelle “*sexy move*” une série particulière de 4 mouvements qui sont faciles à enchainer l’un après l’autre.
cette série sera très utile dans la résolution du cube.

elle peut se faire à droite ou à gauche.

réalisée 6 fois d’affilée du même côté, elle remet le cube dans sa situation de départ.

### version droite

tenir le cube avec le pouce gauche sur la face avant et le pouce droit sur la face du dessous.
index, majeur et annulaire sont chacun sur une pièce sur le bord du cube, l’un à côté de l’autre, sur la face opposée à celle du pouce.
chaque mouvement est un quart de tour d’une face.
les 3 premiers mouvements se font avec la main droite, et le dernier avec la main gauche.

1.  lever la face de droite (la faire tourner dans le sens horloger/anti-trigonométrique).
2.  faire tourner la face du dessus dans le sens horloger/anti-trigonométrique en la tirant avec l’index droit.
3.  abaisser la face de droite (la faire tourner dans le sens anti-horloger/trigonométrique).
4.  faire tourner la face du dessus dans le sens anti-horloger/trigonométrique en la tirant avec l’index gauche.

en utilisation la notation conventionnelle, cette série de mouvement s’écrit :

**R U R' U'**

### version gauche

la version gauche est la version symétrique de la version droite : on inverse gauche et droite et le sens des rotations.
tenir le cube avec le pouce droit sur la face avant et le pouce gauche sur la face du dessous.
index, majeur et annulaire sont chacun sur une pièce sur le bord du cube, l’un à côté de l’autre, sur la face opposée à celle du pouce.
les 3 premiers mouvements se font avec la main gauche, et le dernier avec la main droite.

1.  lever la face de gauche (la faire tourner dans le sens anti-horloger/trigonométrique).
2.  faire tourner la face du dessus dans le sens anti-horloger/trigonométrique en la tirant avec l’index gauche.
3.  abaisser la face de gauche (la faire tourner dans le sens horloger/anti-trigonométrique).
4.  faire tourner la face du dessus dans le sens horloger/anti-trigonométrique en la tirant avec l’index droit.

en utilisation la notation conventionnelle, cette série de mouvement s’écrit :

**L' U' L U**

## étape 1 : la croix blanche

la 1re étape pour résoudre le cube consiste à former une croix blanche en amenant les 4 arêtes blanches sur la face blanche (autour du centre blanc).
les arêtes ayant 2 couleurs, il faut également veiller à les placer pour que leur 2^e^ couleur corresponde à l’autre centre qu’elles touchent.
si c’est déjà le cas, passer à l’étape suivante.

cette étape se fait de manière assez intuitive, mais voici quelques conseils :

1.  tenir le cube de manière à ce que la face blanche (celle avec le centre blanc) soit au-dessus.
2.  s’il y a déjà une arête sur la face blanche, tourner cette face pour que l’autre couleur touche le centre de la même couleur.
3.  s’il y a plusieurs arêtes sur la face blanche, pour chacune dont la couleur ne correspond pas à l’autre centre, tourner la face latérale sur laquelle elles se trouvent d’un demi-tour pour les amener sur la face du dessous (jaune).
4.  s’il y a une arête avec une facette blanche sur la face du dessous (jaune), tourner cette face jusqu’à ce que son autre couleur touche le centre de même couleur.
    ceci amène l’arête en-dessous de son emplacement sur la face blanche.
    ensuite, tourner la face latérale sur laquelle elle se trouve d’un demi-tour pour l’amener à son emplacement.
5.  s’il y a des arêtes sur les côtés latéraux, tourner la face d’un quart de tour pour les amener sur la face du dessous, de manière à ce que la facette blanche touche le centre jaune, et passer au point 4.
6.  s’il y a des arêtes sur la face du dessus ou du dessous mais avec la facette blanche sur le côté, tourner la face contenant la facette blanche d’un quart de tour et passer au point 5.
7.  en tournant les faces latérales, attention à ne pas déplacer de pièce de la croix blanche qui est déjà bien positionnée.
    il est toujours possible de tourner la face blanche temporairement pour éviter cela, et de la remettre en place après le mouvement.

## étape 2 : les sommets blancs

la 2^e^ étape consiste à placer les 4 sommets avec une facette blanche à leur emplacement final.
si c’est déjà le cas, passer à l’étape suivante.

1.  tenir le cube avec la face blanche en-dessous (jaune au-dessus).
2.  repérer un sommet avec une facette blanche sur l’étage supérieur.
3.  identifier les 2 autres couleurs du sommet.
4.  tourner la face du dessus (jaune) de manière à amener le sommet à l’intersection des faces de ses autres couleurs, au-dessus de son emplacement final sur la face du dessous.
5.  tenir le cube de manière à ce que le sommet soit en haut à droite.
6.  faire un ou plusieurs *sexy moves* à droite jusqu’à ce que le sommet soit bien placé, avec la facette blanche vers le bas.
7.  continuer au point 2.
8.  si un sommet est sur l’étage inférieur mais mal placé, tenir le cube de manière à ce que le sommet soit en bas à droite, faire un *sexy move* à droite et passer au point 3.

## étape 3 : le 2^e^ étage

la 3^e^ étape consiste à placer les 4 arêtes latérales (sans facette blanche ni jaune) à leur emplacement final.
si c’est déjà le cas, passer à l’étape suivante.

1.  repérer une arête sans facette jaune sur l’étage supérieur.
2.  tourner la face du dessus (jaune) de manière à ce que la couleur latérale de l’arête touche le centre de la même couleur.
3.  tenir le cube pour avoir la facette latérale et le centre correspondant face à soi.
4.  tourner la face du dessus d’un quart de tour de manière à éloigner l’arête de la face correspondant à sa couleur supérieure.
5.  faire un *sexy move* du côté opposé à l’arête : si l’arête est partie à droite au point 4, faire le *sexy move* à gauche, et inversement.
6.  tourner le cube (pas les faces) d’un quart de tour de manière à amener la face latérale (qui était du côté du *sexy move*) devant.
7.  faire un *sexy move* de l’autre côté qu’au point 5.
8.  continuer au point 1.
9.  si une arête est sur le 2^e^ étage mais mal positionnée, faire la même manipulation pour amener une arête avec une facette jaune à cet emplacement, puis continuer au point 1.

## étape 4 : la croix jaune

la 4^e^ étape consiste à former une croix jaune en orientant correctement les 4 arêtes restantes.
si c’est déjà le cas, passer à l’étape suivante.

1.  regarder la face jaune en ignorant les sommets.
2.  si 2 arêtes sont déjà placées de manière à former une ligne jaune (de 3 facettes), tenir le cube de manière à ce que la ligne soit horizontale (de gauche à droite) et passer au point 7.
3.  s’il n’y a pas de ligne jaune, mais qu’il y a un petit “L” jaune, formé par le centre et 2 autres arêtes, tenir le cube de manière à ce que ces 2 autres arêtes soient sur les faces arrière et gauche et passer au point 5.
4.  s’il n’y a ni ligne jaune ni “L” jaune, l’orientation de la face jaune n’a pas d’importance.
5.  tourner la face de devant d’un quart de tour dans le sens horloger/anti-trigonométrique (**F**), faire 2 *sexy moves* à droite, puis remettre la face de devant en place en la tournant d’un quart de tour dans le sens anti-horloger/trigonométrique (**F'**).
6.  s’il y a une croix jaune, c’est terminé, sinon passer au point 2.
7.  tourner la face de devant d’un quart de tour dans le sens horloger/anti-trigonométrique (**F**), faire un *sexy move* à droite, puis remettre la face de devant en place en la tournant d’un quart de tour dans le sens anti-horloger/trigonométrique (**F'**).

## étape 5 : placer les arêtes

la 5^e^ étape consiste à déplacer les arêtes pour que leur facette latérale touche le centre de la même couleur.

1.  tourner la face du dessus (jaune) de manière à ce que 2 arêtes touchent le centre de la même couleur que leur facette qui n’est pas jaune.
2.  si les 4 arêtes touchent chacune leur centre correspondant, c’est terminé, passer à l’étape suivante.
3.  si les 2 arêtes touchant leur centre correspondant sont l’une à côté de l’autre, tenir le cube de manière à ce que ces arêtes soient sur les faces arrière et gauche et passer au point 5.
4.  les 2 arêtes sont sur des faces opposées.
    il faudra faire la manipulation 2 fois.
    l’orientation de la face jaune n’a pas d’importance pour la 1re fois.
5.  cette manipulation s’appelle “le garage”.
    on va sortir une voiture d’un garage, lui faire faire un petit tour puis la faire rentrer dans le garage.
    la voiture est représentée par les deux facettes l’une au-dessus de l’autre en bas à droite de la face de devant.
6.  ouvrir le garage et amener la voiture sur la face du dessus en levant la face de droite (**R**).
7.  tourner la face du dessus (jaune) d’un demi-tour (**U2**) pour amener la voiture de l’autre côté.
8.  fermer le garage en rabaissant la face de droite (**R'**).
9.  ramener la voiture devant le garage en tournant la face du dessus d’un quart de tour dans le sens anti-horloger/trigonométrique (**U'**)
10. ouvrir le garage en levant la face de droite (**R**).
11. faire entrer la voiture dans le garage en tournant la face du dessus d’un quart de tour dans le sens anti-horloger/trigonométrique (**U'**)
12. fermer le garage en rabaissant la face de droite (**R'**).
13. tourner la face du dessus d’un quart de tour dans le sens anti-horloger/trigonométrique pour réaligner les arêtes (**U'**).
14. passer au point 1.

## étape 6 : placer les sommets

la 6^e^ étape consiste à déplacer les sommets pour qu’ils soient tous à l’intersection des faces correspondant à leur 3 couleurs, indépendamment de leur orientation.

1.  repérer un sommet qui est déjà à sa place (indépendamment de son orientation).
    s’il n’y en a pas, passer au point 3.
    si les 4 sommets sont déjà bien placés, c’est terminé, passer à l’étape suivante.
2.  tenir le cube de manière à ce que ce sommet soit en haut à droite.
3.  cette manipulation s’appelle “les potes”.
    les 2 potes sont représentés par les deux facettes l’une au-dessus de l’autre en bas à gauche et à droite de la face de devant.
    ils vont sortir de chez eux, se faire un *high five*, puis rentrer chez eux.
4.  lever la face de gauche pour sortir le pote de gauche de chez lui (**L'**).
5.  tourner la face du dessus d’un quart de tour dans le sens horloger/anti-trigonométrique (**U**).
6.  lever la face de droite pour sortir le pote de droite de chez lui et faire un *high five* à l’autre (**R**).
7.  tourner la face du dessus d’un quart de tour dans le sens anti-horloger/trigonométrique (**U'**).
8.  abaisser la face de gauche pour que le pote de gauche rentre chez lui (**L**).
9.  tourner la face du dessus d’un quart de tour dans le sens horloger/anti-trigonométrique (**U**).
10. abaisser la face de droite pour que le pote de droite rentre chez lui (**R'**).
11. tourner la face du dessus d’un quart de tour dans le sens anti-horloger/trigonométrique pour réaligner les arêtes (**U'**).
12. passer au point 1.

## étape 7 : orienter les sommets

la 7^e^ et dernière étape consiste à orienter les sommets pour que leur facette jaune soit sur la face jaune.

1.  retourner le cube pour avoir la face blanche au-dessus (jaune en-dessous).
2.  tenir le cube de manière à avoir un sommet mal orienté en bas à droite.
3.  faire une ou plusieurs séries de 2 *sexy moves* à droite jusqu’à ce que le sommet soit bien orienté (facette jaune vers le bas).
    attention à toujours bien faire des *sexy moves* complets (4 mouvements).
4.  sans changer l’orientation du cube (important !), tourner la face du dessous (la jaune) pour amener un autre sommet mal orienté en bas à droite.
5.  s’il n’y en a plus, passer au point suivant, sinon au point 3.
6.  tourner la face du dessous (la jaune) jusqu’à ce que la couleur du bord corresponde à celle de la face.
7.  c’est terminé, le rubik’s cube est résolu, bravo !
